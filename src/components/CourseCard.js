import { useState } from "react";
import { Row, Col, Card, Button } from "react-bootstrap"

export default function CourseCard({courseProp}){
  // useState hook:
  // A hook in React is a kind of tool. The useState hook allows creation and maipulation of states
  // States are a way for React re-renders ONLY the specific component or part of the component that changed(and not the entire page or components whose states have not changed)

  // array destructuring to get the state and the setter
  const [count, setCount] = useState(0)
  const [seats, setSeats] = useState(10)
  // Syntax:
  // const [state, setState] = useState(0)
  // state: a special kind of variable (can be named anything) that React uses to render/re-render components when needed
  // state setter: only way to change a state's value

  // default state: the state's initial value on component mount
  // Before component mounts, a state actually defaults to undefined, then is changed to its default state

  // By convention they are named after the state
  let { name, description, price } = courseProp;

  function enroll(){
    setCount(count+1);
    console.log("Enrollees: " + count)
    setSeats(seats-1)
    if (seats === 0){
      alert("No more seats available")
      setCount(10)
      setSeats(0)
    }
  }

  /*
    ACTIVITY:
    Create a state hook that will represent the number of available seats for each course

    It should default to 10, and decrement by 1 each time a student enrolls

    Add a condition that will show an alert that no more seats are available if the seats state is 0
  */


  return(
    <Row>
      <Col xs={12} md={12} class="courseCard1">
      <Card className="cardCourseCard p-12 ">
        <Card.Body>
          <Card.Title><b>{name}</b></Card.Title>
          <Card.Subtitle>Description:
          </Card.Subtitle>
          <Card.Text>{description}</Card.Text>
          <Card.Subtitle>Price:</Card.Subtitle>
          <Card.Text><span>&#8369;</span>{price}</Card.Text>
          <Card.Text>Enrollees: {count}</Card.Text>
          <Card.Text>Available Seats: {seats}</Card.Text>
          <Button variant="primary" onClick={enroll}>Enroll Now</Button>
        </Card.Body>

        </Card>
      </Col>
    </Row>
        
  )
}

