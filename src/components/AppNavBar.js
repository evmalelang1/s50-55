// import Navbar from "react-bootstrap/Navbar";
// import Nav from "react-bootstrap/Nav";

//destructuring - we just get what we need
import {Navbar, Nav, Container} from "react-bootstrap";

export default function AppNavBar(){
  return (
    <Navbar bg="light" expand="lg">
      <Container>
        <Navbar.Brand href="#home">B211 Course Booking App</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link href="#home">Home</Nav.Link>
            <Nav.Link href="#link">Courses</Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  )
}

