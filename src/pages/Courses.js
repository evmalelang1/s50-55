import coursesData from '../data/coursesData';
import CourseCard from '../components/CourseCard';

export default function Courses(){

  // using the map array method, we can loop through our courseData and dynamically render any number of CourseCards depending on how many array elements are present in our data

  // map returns an array, which we can display in the page via the component's return statement

  // props are a way to pass any valid JS Data from parent component to child component

  // you can pass as many props as you want. Even functions can be passed as props
  const courses = coursesData.map((course)=>{
    // console.log(course)
    return <CourseCard courseProp={course} key={course.id}/>
  })

  // console.log(coursesData)
  return(
    <>
    {courses}
    </>
    
  )
}