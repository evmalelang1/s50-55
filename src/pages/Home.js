import Banner from "../components/Banner";
// import CourseCard from "../components/CourseCard";
import Highlights from "../components/Highlights";
import { Container } from "react-bootstrap";


export default function Home(){
  return(
    <>
    <Banner/>
    <Highlights/>
    <Container>
    {/* <CourseCard/> */}
    </Container>
    </>
  )
}