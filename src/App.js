// import {Fragment} from "react";
import { Container } from "react-bootstrap";
import './App.css';
import AppNavBar from "./components/AppNavBar";
import Home from "./pages/Home";
import Courses from "./pages/Courses"

function App() {
  return (
    <>
      <AppNavBar/>
      <Container>
        <Home/>
        <Courses/>
      </Container>
    </>

  );
  
}

export default App;
